import Vue from 'vue'
import { Button, Form, FormItem, Input, Message, container, header, aside, main, MessageBox, Tag, Step,Steps,Upload, Timeline, TimelineItem} from 'element-ui'
// 导入弹框提示组件
Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(container)
Vue.use(header)
Vue.use(aside)
Vue.use(main)
Vue.use(Tag)
Vue.use(Step)
Vue.use(Steps)
Vue.use(Upload)
Vue.use(Timeline) 
Vue.use(TimelineItem)
Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox.confirm