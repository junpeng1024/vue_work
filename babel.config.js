// 这是项目发布阶段需要用到的 babel 插件
const productPlugins = []
if(process.env.NODE_ENV === 'production'){
  // 发布阶段
  productPlugins.push("transform-remove-console")
}

module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  // 发布产品时候用到的插件数组
  plugins: [
    ...productPlugins,
    // 配置路由懒加载插件
    "@babel/plugin-syntax-dynamic-import"
  ]
}
